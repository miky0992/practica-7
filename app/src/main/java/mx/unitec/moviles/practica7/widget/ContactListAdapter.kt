package mx.unitec.moviles.practica7.widget

import android.content.Context
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mx.unitec.moviles.practica7.R
import mx.unitec.moviles.practica7.model.Contact

class ContactListAdapter internal constructor(context: Context) :
    RecyclerView.Adapter<ContactListAdapter.ContactViedHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var contacts = listOf<Contact>()

    inner class ContactViedHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        val contactNameItemView: TextView = itemview.findViewById(R.id.ttvName)
        val contactPhoneItemView: TextView = itemview.findViewById(R.id.ttvPhone)
        val contactEmailItemView: TextView = itemview.findViewById(R.id.ttvEmail)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViedHolder {
        val itemview = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return ContactViedHolder(itemview)
    }

    override fun getItemCount() = contacts.size

    override fun onBindViewHolder(holder: ContactViedHolder, position: Int) {
        val current = contacts[position]
        holder.contactNameItemView.text = current.name
        holder.contactEmailItemView.text = current.email
        holder.contactPhoneItemView.text = current.phone
    }

    internal fun setContacts(contacts: List<Contact>) {
        this.contacts = contacts
        notifyDataSetChanged()
    }

    internal fun getContactAt(position: Int) : Contact {
        return contacts[position]

    }

}